// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyDJe-fWTjxW7yKmO_7K7iPL1ExvVzDguIE",
    authDomain: "aliadosapp.firebaseapp.com",
    projectId: "aliadosapp",
    storageBucket: "aliadosapp.appspot.com",
    messagingSenderId: "632768941917",
    appId: "1:632768941917:web:cd34f37a5be05dcaf7e2b4",
    measurementId: "G-6Y066J0WQ9"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
