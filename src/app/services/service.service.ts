import { EventEmitter, Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from "@angular/fire/firestore";
import { AngularFireStorage } from "@angular/fire/storage";
import { Router } from '@angular/router';
import firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  credential: any = ''

  constructor(
    private afs: AngularFirestore,
    private afa: AngularFireAuth,
    private router: Router,
    private _afs: AngularFireStorage) { }

  register(n: string, e: string, p: string, c: string = '', us: boolean = false) {
    var user = ''
    var coll = ''
    return this.afa.createUserWithEmailAndPassword(e, p)
      .then((u) => {
        if (!us) {
          user = 'usuario'
          coll = 'Usuarios'
        } else {
          user = c
          coll = 'Empresa'
        }
        this.afs.collection(coll).doc(u.user?.uid).set({
          Name: n,
          Email: e,
          Company: c,
          CuentaTipo: "",
          Banco: ""
        }).then(() => {
          u.user?.updateProfile({
            displayName: user
          }).then(() => {
            u.user?.sendEmailVerification()
          }).then(() => {
            this.router.navigateByUrl('/Tablero')
          })
        })
      })
  }

  logIn(u: string, p: string) {
    this.credential = firebase.auth.EmailAuthProvider.credential(u, p)
    return this.afa.signInWithEmailAndPassword(u, p)
  }

  out() {
    this.afa.signOut()
    this.router.navigateByUrl('/Login')
  }

  getType() {
    return this.afa.authState
  }

  getInfoC(c: string) {
    return this.afs.collection(c)
  }

  resetPassword() {
    return this.afa
  }

  getUser() {
    return this.afa.currentUser
  }

  //Funciones crud

  newC(n: string, e: string, t: string, em: string, a: string, id: string) {
    this.afs.collection('Clientes').add({
      nombre: n,
      email: e,
      telefono: t,
      empresa: em,
      infoAd: a,
      refiere: id,
      estado: 'Pendiente',
      creation: new Date()
    })
  }

  updateInfoProfile(id: string, n: string, t: string, e: string, ced: any, c: string, b: string, ct: string, tc: string, a: string) {
    return this.getInfoC('Usuarios').doc(id).update({
      Name: n,
      Telefono: t,
      Company: e,
      Identificacion: ced,
      NoCuenta: c,
      Banco: b,
      CuentaTipo: ct,
      TitularC: tc,
      Autorizo: a
    })
  }

  updateDocs(id: string, f: any){
    return this.getInfoC('Usuarios').doc(id).update({
      Identificacion: f
    })
  }

  updateCustomer(e: any) {
    var temp: string = ''
    if (!e.id || e.id == undefined) {
      temp = e.idC
    } else {
      temp = e.id
    }
    return this.getInfoC('Clientes').doc(temp).update({
      email: e.email,
      empresa: e.empresa,
      estado: e.estado,
      infoAd: e.infoAd,
      nombre: e.nombre,
      telefono: e.telefono,
      valorVenta: e.valorVenta,
      comisionVenta: e.comisionVenta,
      idC: temp,
      creation: e.creation
    })
  }

  uploadFile(e: any, uid: string) {
    return this._afs.ref("Usuarios/" + uid + "/" + e.name).put(e)
  }

  deleteFile(uid: string, name: string) {
    return this._afs.ref("Usuarios/" + uid + "/" + name).delete()
  }

}
