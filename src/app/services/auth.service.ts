import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { CanActivate, Router } from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate {

  r: boolean = false

  constructor(private auth: AngularFireAuth, private router: Router) { }

  canActivate(): boolean {
    this.auth.onAuthStateChanged((u) => {
      if (!u) {
        this.router.navigateByUrl('/Login')
      }
    })
    return true
  }

  
}
