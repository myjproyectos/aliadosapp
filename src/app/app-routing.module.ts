import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './components/admin/admin/admin.component';
import { AsesoresComponent } from './components/admin/asesores/asesores.component';
import { GananciasComponent } from './components/admin/ganancias/ganancias.component';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { SettingsComponent } from './components/settings/settings.component';
import { AuthService as AuthGuard } from "./services/auth.service";

const routes: Routes = [
  // { path: 'Home', component: HomeComponent },
  { path: 'Login', component: LoginComponent },
  { path: 'Register/:id', component: RegisterComponent },
  { path: 'Tablero', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'Ajustes', component: SettingsComponent, canActivate: [AuthGuard] },
  { path: 'Admin', component: AdminComponent, canActivate: [AuthGuard], 
    children: [
      { path: 'Ganancias', component: GananciasComponent, canActivate: [AuthGuard] },
      { path: 'Asesores', component: AsesoresComponent, canActivate: [AuthGuard] },
      { path: '**', redirectTo: 'Ganancias', pathMatch: 'full' }
    ]},
  { path: '**', redirectTo: 'Login', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
