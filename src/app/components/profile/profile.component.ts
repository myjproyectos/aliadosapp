import { Component, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ServiceService } from 'src/app/services/service.service';
declare var $: any

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy, OnChanges {

  _p: string = ''
  _name: string = ''
  e: any = new RegExp(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,9}$/)
  _doc: any = ''
  _info: any[] = []
  _acUser: any = ''
  _toast: boolean = false
  _text: string = ''
  _verInfo: boolean = false
  info: any = ''
  _fun: any = ''
  _cont = 0
  _gInfo: boolean = false
  _modal: boolean = false
  //Destruir
  _sub: Subscription
  _sub2: Subscription

  constructor(private _s: ServiceService) {
    this._sub = new Subscription()
    this._sub2 = new Subscription()
  }

  ngOnInit(): void {

    $('[data-toggle="tooltip"]').tooltip()

    this._sub = this._s.getType().subscribe((u) => {
      var _f = async () => {
        this._acUser = u
        this._doc = u?.uid
        if (u?.displayName == 'usuario') {
          this._fun = 'Usuarios'
          this._p = 'usuario'
        } else {
          this._fun = 'Empresa'
          this._p = 'empresa'
        }
      }
      _f().then(() => {
        this._s.getInfoC(this._fun).doc(this._doc).snapshotChanges().subscribe((d) => {
          if (d.payload.data()) {
            var _temp: any = d.payload.data()
            this._name = _temp.Name
          }
        })
        this.ngOnChanges()
      })
    })
  }

  newC(n: string, e: string, t: string, em: string, a: string) {
    this._s.newC(n, e, t, em, a, this._doc)
  }

  editInfo(e: any) {
    this.info = e
    this._verInfo = true
  }

  fillInfo(e: any) {
    this.info = e
  }

  ngOnChanges() {
    this._sub2 = this._s.getInfoC('Clientes').snapshotChanges().subscribe((u) => {
      this._info = []
      u.map((m) => {
        var _t: any = m.payload.doc.data()
        if (_t.refiere == this._doc && this._fun == 'Usuarios') {
          this._info.push(_t)
        }
        if (this._fun == 'Empresa') {
          var _f = async () => {
            this._info.push(_t)
          }
          _f().then(() => {
            if (this._info[this._cont]) {
              this._info[this._cont].id = m.payload.doc.id
              this._cont++
            }
          })
        }
      })
    })
  }

  ngOnDestroy() {
    this._sub.unsubscribe()
    this._sub2.unsubscribe()
  }

}
