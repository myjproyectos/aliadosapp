import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ServiceService } from 'src/app/services/service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  type: any = ''
  _eye: boolean = false
  us: boolean = false
  _textE: string = 'El correo electrónico ya se encuentra en uso. Inicia sesión'
  _toast: boolean = false
  _err: boolean = true
  load: boolean = false
  //Validar email
  e: any = new RegExp(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,9}$/)

  constructor(private s: ServiceService, private router: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.type = this.router.snapshot.paramMap.get('id')
  }

  register(n: string, e: string, p: string, c: any = '') {
    this.load = true
    if (this.type == '$Gx$!y8Adhv0') {
      this.us = true
    }
    this.s.register(n, e, p, c, this.us)
    .catch((e)=>{
      if(e.code == 'auth/email-already-in-use'){
        this._toast = true
        setTimeout(() => {
          this._toast = false
        }, 5000);
      }
    })
  }

}
