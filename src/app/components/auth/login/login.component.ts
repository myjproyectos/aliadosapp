import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { ServiceService } from 'src/app/services/service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  e: any = new RegExp(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,9}$/)
  _toast: boolean = false
  _text: string = ''
  _err: boolean = false

  constructor(private _s: ServiceService, private _ro: Router) { }

  ngOnInit(): void {
  }

  logIn(u: string, p: string) {
    this._s.logIn(u, p)
      .then(() => this._ro.navigateByUrl('/Tablero'))
      .catch((e) => {
        if (e.code == "auth/user-not-found") {
          this._text = 'El correo electrónico no se encuentra registrado'
        }
        if (e.code == "auth/wrong-password") {
          this._text = 'Contraseña incorrecta'
        }
        this._err = true
        this.funToast()
      })
  }

  forgotPass(e: string) {
    var _f = async () => {
      this._s.resetPassword().sendPasswordResetEmail(e)
        .then(() => {
          this._text = 'Se ha enviado un correo electrónico para reestablecer tu contraseña'
        })
        .catch(() => {
          this._err = true
          this._text = 'El correo electrónico no se encuentra registrado'
        })
    }
    _f().then(() => {
      this.funToast()
    })
  }

  funToast() {
    this._toast = true
    setTimeout(() => {
      this._toast = false
    }, 5000);
  }
}
