import { Component, OnChanges, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from 'src/app/services/service.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styles: [
  ]
})
export class AdminComponent implements OnInit, OnChanges {

  constructor(private _s: ServiceService, private router: Router) { }

  ngOnInit(): void {
    this._s.getType().subscribe((u) => {
      if (u?.email != 'manuelitoys93@gmail.com') {
        this.router.navigateByUrl('Tablero')
      }
    })
  }

  ngOnChanges() {
   
  }

}
