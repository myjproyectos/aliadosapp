import { Component, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ServiceService } from 'src/app/services/service.service';

@Component({
  selector: 'app-ganancias',
  templateUrl: './ganancias.component.html',
  styles: [
  ]
})
export class GananciasComponent implements OnInit, OnChanges, OnDestroy {

  clientes: any[] = []
  mNow = new Date()
  _sub: Subscription

  constructor(private _s: ServiceService) {
    this._sub = new Subscription()
  }

  ngOnInit(): void {
    this._sub = this._s.getInfoC('Clientes').snapshotChanges()
      .subscribe((c) => {
        c.map((m) => {
          var _t: any = m.payload.doc.data()
          var _d = new Date(_t.creation.toDate())
          if (this.mNow.getMonth() == _d.getMonth() && this.mNow.getFullYear() == _d.getFullYear() && _t.estado == 'Facturado') {
            this._s.getInfoC('Usuarios').doc(_t.refiere).snapshotChanges()
              .subscribe((u) => {
                var _f = async () => {
                  var _u: any = u.payload.data()
                  _t.nombre = _u.Name
                  _t.email = _u.Email
                  _t.idU = u.payload.id
                }
                _f().then(() => {
                  this.clientes.push(_t)
                })
              })
          }
        })
      })
  }

  ngOnChanges(): void {

  }

  ngOnDestroy() {
    this._sub.unsubscribe()
  }

}
