import { Component, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ServiceService } from 'src/app/services/service.service';

declare var $: any

@Component({
  selector: 'app-asesores',
  templateUrl: './asesores.component.html',
  styles: [
  ]
})
export class AsesoresComponent implements OnInit, OnDestroy, OnChanges {

  asesores: any[] = []
  _sub: Subscription
  modal: boolean = false
  _t: boolean = false
  _text: string = 'Asesor agregado correctamente'
  _eye: boolean = false

  constructor(private _s: ServiceService) {
    this._sub = new Subscription()
  }

  ngOnInit(): void {
    this._sub = this._s.getInfoC('Empresa').snapshotChanges().subscribe((d) => {
      d.forEach(m => {
        this.asesores.push(m.payload.doc.data())
      })
    })
  }

  modalC(){
    if(this.modal){
      $('body').addClass('modal-open')
      $("#staticBackdrop").fadeIn()
    }else{
      $('body').removeClass('modal-open')
      $("#staticBackdrop").fadeOut()
    }
  }

  addAsesor(n: string, e: string, p:string, c: string){
    this._s.register(n, e, p, c, true).then(()=>{
      this._t = true
      setTimeout(() => {
        this._t = false
      }, 5000);
    })
  }

  ngOnChanges(){}

  ngOnDestroy() {
    this._sub.unsubscribe()
  }

}
