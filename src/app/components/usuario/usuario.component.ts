import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.scss']
})
export class UsuarioComponent implements OnInit, OnChanges {

  @Input() _data: any = {}
  @Output() _b: EventEmitter<boolean>
  _total: number = 0

  constructor() { 
    this._b = new EventEmitter()
  }

  ngOnInit(): void {
  }

  ngOnChanges(): void{
    if(this._data != {}){
      this._total = ((this._data.valorVenta*this._data.comisionVenta)/100)-(((this._data.valorVenta*this._data.comisionVenta)/100) * 0.05)
    }
  }

  close(){
    this._b.emit(false)
  }
}
