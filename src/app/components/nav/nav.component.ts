import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ServiceService } from 'src/app/services/service.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit, OnDestroy {

  _t: any = true
  _acUser: any = ''
  _toast: boolean = false
  _text: string = 'Correo enviado correctamente'

  //Destruir
  _dest: Subscription

  constructor(private afa: ServiceService) {
    this._dest = new Subscription()
  }

  ngOnInit(): void {
    this._dest = this.afa.getType().subscribe((u) => {
      this._acUser = u
      this._t = u?.emailVerified
    })
  }

  verify() {
    var _f = async () => {
      this._acUser.sendEmailVerification()
    }
    _f().then(() => {
      this._toast = true
      setTimeout(() => {
        this._toast = false
      }, 5000);
    })
  }

  signOut() {
    this.afa.out()
  }

  ngOnDestroy() {
    this._dest.unsubscribe()
  }

}
