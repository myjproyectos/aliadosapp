import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { ServiceService } from 'src/app/services/service.service';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.scss']
})
export class EmpresaComponent implements OnInit, OnChanges {

  @Input() _infoEmp: any = ''
  @Output() _vI: EventEmitter<boolean>
  @Output() _t: EventEmitter<string>
  @Output() _tst: EventEmitter<boolean>

  _ref: string = ''
  e: any = new RegExp(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,9}$/)
  _text: string = 'Datos actualizados correctamente'

  constructor(private _s: ServiceService) {
    this._vI = new EventEmitter()
    this._t = new EventEmitter()
    this._tst = new EventEmitter()
   }

  ngOnInit(): void {
  }

  ngOnChanges() {
    if (this._infoEmp != '') {
      this._s.getInfoC('Usuarios').doc(this._infoEmp.refiere).get()
        .subscribe((u) => {
          var t: any = u.data()
          this._ref = t.Name
        })
    }
  }

  updateInfo(n: string, e: string, t: string, c: string, a: string, s: string, v: number, co: number) {
    var _f = async () => {
      this._infoEmp.nombre = n
      this._infoEmp.email = e
      this._infoEmp.telefono = t
      this._infoEmp.empresa = c
      this._infoEmp.infoAd = a
      this._infoEmp.estado = s
      this._infoEmp.valorVenta = this._infoEmp.estado != 'Facturado' ? '' : v
      this._infoEmp.comisionVenta = this._infoEmp.estado != 'Facturado' ? '' : co
    }
    _f().then(() => {
      this._s.updateCustomer(this._infoEmp)
        .then(() => {
          this._vI.emit(false)
          this._tst.emit(true)
          this._t.emit(this._text)
          setTimeout(() => {
            this._tst.emit(false)
          }, 5000);
        })
    })
  }

  back(){
    this._vI.emit(false)
  }
}
