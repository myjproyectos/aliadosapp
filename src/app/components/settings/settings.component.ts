import { Component, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ServiceService } from 'src/app/services/service.service';
import { Router } from "@angular/router";
declare var $: any

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy, OnChanges {

  data: any = ''
  info: any = ''
  toast: boolean = false
  text: string = ''
  ced: any[] = []
  load: boolean = false
  filesT: any[] = []
  //Eventos modal
  modal: boolean = false
  position: number = 0
  nameT: string = ''

  //Destruir
  _sub: Subscription

  constructor(private _s: ServiceService, private router: Router) {
    this._sub = new Subscription()
  }

  ngOnInit(): void {
    $('[data-toggle="tooltip"]').tooltip()

    this._sub = this._s.getType().subscribe((u) => {
      this.info = u
      if (u?.displayName == 'usuario') {
        this._s.getInfoC('Usuarios').doc(u?.uid).snapshotChanges().subscribe((d) => {
          this.data = d.payload.data()
          if (this.data.Identificacion) {
            this.filesT = this.data.Identificacion
          }
        })
      }
    })
  }

  updateInfo(n: string, t: string, e: string, c: string, b: string, ct: string, tc: string, a: string) {
    this.load = true
    var cont: number = 1
    this._s.getUser().then((us: any) => {
      var prom = () => {
        return new Promise<void>((r, e) => {
          if (this.filesT.length > 0 && this.ced.length == 0) {
            r()
          } else {
            for (const e of this.ced) {
              this._s.uploadFile(e, us.uid).then((u) => {
                u.ref.getDownloadURL().then((url) => {
                  this.filesT.push({ "nameC": e.name, "url": url })
                }).then(() => {
                  if (cont == this.ced.length) {
                    r()
                  }
                  cont++
                })
              })
            }
          }
        })
      }
      prom().then(() => {
        this._s.updateInfoProfile(this.info.uid, n, t, e, this.filesT, c, b, ct, tc, a).then(() => {
          this.load = false
          this._toast()
        })
      })

    })
  }

  updatePass(p: string) {
    this._s.resetPassword().currentUser.then((u) => {
      u?.reauthenticateWithCredential(this._s.credential)
        .then(() => {
          u?.updatePassword(p).then(() => {
            this._toast()
          })
        })
    })
  }

  deleteDoc(p: number) {
    this.filesT.splice(p, 1)
    this._s.deleteFile(this.info.uid, this.nameT).subscribe(() => {
      this._s.updateDocs(this.info.uid, this.filesT)
        .then(() => {
          this.modal = false
          this._toast('Documento eliminado correctamente')
        })
    })
  }

  _toast(t?: string) {
    this.text = 'Datos actualizados correctamente'
    if (t) { this.text = t }
    this.toast = true
    setTimeout(() => {
      this.toast = false
    }, 5000);
  }

  changeCed(e: any){
    this.ced = e.target.files
  }

  ngOnChanges() {

  }

  ngOnDestroy() {
    this._sub.unsubscribe()
  }
}
