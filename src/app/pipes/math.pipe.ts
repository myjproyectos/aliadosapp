import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'math'
})
export class MathPipe implements PipeTransform {

  transform(value: number, args: number, ar2?: number): number {
    var _total: number = (value * args) / 100
    if ( !ar2 ) {
      return _total
    } else {
      var _g: number = _total - (_total - (_total * ar2))
      return _g
    }
  }

}
