import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//Firebase connection
import { AngularFireModule } from "@angular/fire";
import { environment } from "../environments/environment";
//Firebase authentication
import { AngularFireAuthModule } from "@angular/fire/auth";
//Firebase comunication with DB
// import { AngularFirestoreModule } from "@angular/fire/firestore";

//Angular Forms
import { FormsModule } from "@angular/forms";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './components/nav/nav.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ServiceService } from './services/service.service';
import { SettingsComponent } from './components/settings/settings.component';
import { ToastComponent } from './components/toast/toast.component';
import { EmpresaComponent } from './components/empresa/empresa.component';
import { UsuarioComponent } from './components/usuario/usuario.component';
import { AdminComponent } from './components/admin/admin/admin.component';
import { MathPipe } from './pipes/math.pipe';
import { FooterComponent } from './components/footer/footer.component';
import { GananciasComponent } from './components/admin/ganancias/ganancias.component';
import { AsesoresComponent } from './components/admin/asesores/asesores.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent,
    SettingsComponent,
    ToastComponent,
    EmpresaComponent,
    UsuarioComponent,
    AdminComponent,
    MathPipe,
    FooterComponent,
    GananciasComponent,
    AsesoresComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    // AngularFirestoreModule,
    FormsModule
  ],
  providers: [ServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
